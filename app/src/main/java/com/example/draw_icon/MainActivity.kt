package com.example.draw_icon


import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MainScreen()
        }
    }
}

@Composable
fun MainScreen() {
    Column(
        modifier = Modifier
            .padding(24.dp)
            .fillMaxSize()
            .wrapContentSize(align = Alignment.TopCenter)) {


        InstagramIcon()
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = "Instagram",
            fontSize = 20.sp,
            textAlign = TextAlign.Center)

        MessengerIcon()
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = "Messenger",
            fontSize = 20.sp,
            textAlign = TextAlign.Center
        )

        WeatherAppIcon()
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = "Weather App",
            fontSize = 20.sp,
            textAlign = TextAlign.Center
        )
    }
}

@Composable
fun WeatherAppIcon() {
    val backgroundColor = listOf(Color(0xFF2078EE), Color(0xFF74E6FE))
    val sunColor = listOf(Color(0xFFFFC200), Color(0xFFFFE100))

    Canvas(
        modifier = Modifier
            .size(100.dp)
            .padding(16.dp),
        onDraw = {
            val width = size.width
            val height = size.height

            val path = Path().apply {
                moveTo(x = width * 0.76f, y = height * 0.72f)

                cubicTo(
                    x1 = width * 0.93f, y1 = height * 0.72f,
                    x2 = width * 0.98f, y2 = height * 0.41f,
                    x3 = width * 0.76f, y3 = height * 0.40f
                )
                cubicTo(
                    x1 = width * 0.75f, y1 = height * 0.21f,
                    x2 = width * 0.35f, y2 = height * 0.21f,
                    x3 = width * 0.38f, y3 = height * 0.50f
                )
                cubicTo(
                    x1 = width * 0.25f, y1 = height * 0.50f,
                    x2 = width * 0.20f, y2 = height * 0.69f,
                    x3 = width * 0.41f, y3 = height * 0.72f
                )
                close()
            }

            drawRoundRect(
                brush = Brush.verticalGradient(backgroundColor),
                cornerRadius = CornerRadius(50f, 50f),
            )

            drawCircle(
                brush = Brush.linearGradient(sunColor),
                radius = width * 0.17f,
                center = Offset(
                    x = width * 0.35f, y = height * 0.35f
                )
            )
            drawPath(path = path, color = Color.White.copy(alpha = 0.9f))
        }
    )
}

@Composable
fun MessengerIcon() {
    val colorList = listOf(Color(0xFF02b8f9), Color(0xFF0277fe))

    Canvas(
        modifier = Modifier
            .size(100.dp)
            .padding(16.dp),
        onDraw = {

            val trianglePath = Path().let {
                it.moveTo(this.size.width * 0.20f, this.size.height * 0.77f)
                it.lineTo(this.size.width * 0.20f, this.size.height * 0.95f)
                it.lineTo(this.size.width * 0.37f, this.size.height * 0.86f)
                it.close()
                it
            }

            val electricPath = Path().let {
                it.moveTo(this.size.width * 0.20f, this.size.height * 0.60f)
                it.lineTo(this.size.width * 0.45f, this.size.height * 0.35f)
                it.lineTo(this.size.width * 0.56f, this.size.height * 0.46f)
                it.lineTo(this.size.width * 0.78f, this.size.height * 0.35f)
                it.lineTo(this.size.width * 0.54f, this.size.height * 0.60f)
                it.lineTo(this.size.width * 0.43f, this.size.height * 0.45f)
                it.close()
                it
            }

            drawOval(
                Brush.verticalGradient(colors = colorList),
                size = Size(this.size.width, this.size.height * 0.95f)
            )

            drawPath(
                path = trianglePath,
                Brush.verticalGradient(colors = colorList),
                style = Stroke(width = 15f, cap = StrokeCap.Round)
            )

            drawPath(path = electricPath, color = Color.White)

        })


}

@Composable
fun InstagramIcon() {
    val instagramColors = listOf(Color.Yellow, Color.Red, Color.Magenta)
    Canvas(
        modifier = Modifier
            .size(100.dp)
            .padding(16.dp),
        onDraw = {

            drawRoundRect(
                brush = Brush.linearGradient(colors = instagramColors),
                cornerRadius = CornerRadius(60f, 60f),
                style = Stroke(width = 15f, cap = StrokeCap.Round)
            )

            drawCircle(
                brush = Brush.linearGradient(colors = instagramColors),
                radius = 45f,
                style = Stroke(width = 15f, cap = StrokeCap.Round)
            )

            drawCircle(
                brush = Brush.linearGradient(colors = instagramColors),
                radius = 13f,
                center = Offset(
                    x = this.size.width * 0.8f,
                    y = this.size.height * 0.20f
                )
            )
        })
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MainScreen()
}